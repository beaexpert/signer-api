# SIGNER-API

Example how to use the beA.expert SIGNER-API to generate fully qualified signatures (beA, BNotK etc.) 

## Getting started

Read more: https://bea.expert/signer-api/

## License
Open source. To be used with a SIGNER-API-Account.
