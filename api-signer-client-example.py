"""
    beA.expert SIGNER-API / EXPERIMENTAL
    ---------------------------------
    Demo script not intented for production
    Version 1.3 / 21.12.2022 - FOR API 1.3.2.0
    (c) be next GmbH (Licence: GPL-2.0 & BSD-3-Clause)
    https://opensource.org/licenses/GPL-2.0
    https://opensource.org/licenses/BSD-3-Clause

    notice: to use this script you first need to
            subscribe to the beA.expert SIGNER API 
            more info: https://bea.expert/signer-api/
"""
import socket
import json
import os
import platform
from datetime import datetime
from types import SimpleNamespace

MACOS=True if platform.system()=="Darwin" else False
WINDOWS=True if platform.system()=="Windows" else False
UNIX=True if platform.system()=="Linux" else False

if WINDOWS:
    from winreg import *

signer_api_url="127.0.0.1"
signer_api_port=8425

# low level connection to the signer via sockets
def api_connect(command_json_str):     
    global signer_api_url
    global signer_api_port
    debug=False
    if debug: print("api_connect started")
    if not command_json_str=="": # detect an empty command
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # create the SIGNER-API socket
        serversocket.connect((signer_api_url, signer_api_port))  # connect to the SIGNER-API via socket
        if serversocket:
            if "\r\n" not in command_json_str:
                command_json_str+="\r\n" # add LF and CR as socket stopper
            command_data=bytes(command_json_str.encode("utf-8")) # convert "command" string to bytes structure for socket communication
            nbre=serversocket.send(command_data) # send the command to the SIGNER-API
            if(nbre<=0):
                response_json_str='{"error":"can not send command to signer api","error_is_local":true}' # something went wrong
            else:
                if debug: print("api_connect data sent nbre="+str(nbre)) # we sent some bytes
                response_data=b'' # cleanup response buffer
                while True:
                    response_data+=serversocket.recv(2048) # we got a response
                    if not response_data: # empty response
                        break
                    elif len(response_data)>1:
                        if response_data[-1]==10 and response_data[-2]==13: # detect LF and CR to stop receiving
                            break
                if response_data[-1]==10 and response_data[-2]==13: # check if end tag ins't broken before cleanup
                    response_data=response_data[0:-2] # cleanup LF and CR
                serversocket.close() # close properly the socket
                if debug: print("api_connect data received nbre="+str(nbre)) # we received some bytes
                response_json_str=response_data.decode('utf-8') # get the bytes into UTF-8 string
        else:
            response_json_str='{"error":"can not connect to signer api","error_is_local":true}'
        if debug: print("api_connect now ending")
    else:
        response_json_str='{"error":"command is empty","error_is_local":true}'
    return response_json_str # return a proper string that should be a json

# low level get the port
def api_get_port(method):
    port=-1
    lastupdate=""
    execution_without_error=False
    if method=="registry":
        if WINDOWS:
            registry_read_port_str=r'SOFTWARE\bea-suite-signer'        
            try:
                registry_read_port_key=OpenKey(HKEY_CURRENT_USER,registry_read_port_str,KEY_READ)            
            except:            
                execution_without_error=True
            
            if not execution_without_error:
                try:
                    port=QueryValue(registry_read_port_key, "Port")
                except:            
                    execution_without_error=True

            if not execution_without_error:
                try:                
                    lastupdate=QueryValue(registry_read_port_key, "Lastupdate")
                except:            
                    execution_without_error=True
        else:
            execution_without_error=True

    elif method=="file":
        if WINDOWS:
            filename=os.path.join(os.environ['TEMP'],"bea-suite-signer.port")
        else:
            filename=os.path.join(os.environ['TMPDIR'],"bea-suite-signer.port")

        if os.path.exists(filename):
            f=open(filename, "r")
            port=int(f.read())
            f.close()
            lastupdate=datetime.fromtimestamp(os.path.getmtime(filename)).strftime("%d.%m.%Y %H:%M:%S")
        else:
            execution_without_error=True
    else:
        execution_without_error=True

    return execution_without_error, port, lastupdate

# get remote or local signing
def signer_command_get_version():
    debug=False
    execution_without_error=False
    signer_version=""
    update_available=False
    command='{"command":"get_signer_version"}'
    result_str=api_connect(command)    
    if debug: print(result_str)

    result_json=json.loads(result_str)
    if "error" in result_json:
        if debug: print("ERROR: "+result_json['error'])        
    elif "signer_version" in result_json:
        execution_without_error=True
        if debug: print(result_json)
        signer_version=result_json['signer_version']
        if "update_available" in result_json:
            update_available=True
        else:
            update_available=False

    return execution_without_error,signer_version,update_available

# get remote or local signing
def signer_command_get_remote():
    debug=False
    execution_without_error=False
    remote_signing=False
    command='{"command":"get_signing_remote"}'
    result_str=api_connect(command)    
    if debug: print(result_str)

    result_json=json.loads(result_str)
    if "error" in result_json:
        if debug: print("ERROR: "+result_json['error'])        
    elif "signing_remote" in result_json:        
        if result_json['signing_remote']==True:
            remote_signing=True
        execution_without_error=True    
    return execution_without_error,remote_signing

# get the certificate(s)
def signer_command_get_certificates_local_and_remote(remote_signing):
    debug=False
    execution_without_error=False
    if remote_signing:
        command='{"command":"get_certificates_remote"}'
    else:
        command='{"command":"get_certificates_local"}'
    result_str=api_connect(command)
    if debug: print(result_str)

    result_json=json.loads(result_str)
    if "error" in result_json:
        if debug: print("ERROR: "+result_json['error'])
        resp_json=result_json        
    else:
        # load the JSON answer into a PYTHON data structure to parse the certificates   
        if remote_signing:
            resp_json = json.loads(
                    str(json.JSONEncoder().encode(result_json['certificates_remote'])), # remote
                    object_hook=lambda 
                    d: SimpleNamespace(**d)
            )            
        else:
            resp_json = json.loads(
                    str(json.JSONEncoder().encode(result_json['certificates_local'])), # local
                    object_hook=lambda 
                    d: SimpleNamespace(**d)
            )    
        execution_without_error=True
    return execution_without_error, resp_json

# sign the file(s)
def signer_command_sign_files(remote_signing,certificate_to_use,pin,userid,one_document_to_sign_full_path,list_of_documents_to_sign_full_path):
    debug=False
    execution_without_error=False
    documents_to_sign_full_path=""

    if one_document_to_sign_full_path!="":
        documents_to_sign_full_path='"'+one_document_to_sign_full_path+'"'
    else:
        if len(list_of_documents_to_sign_full_path)>0:
            for d in list_of_documents_to_sign_full_path:
                if documents_to_sign_full_path!="":
                    documents_to_sign_full_path+=","
                documents_to_sign_full_path+='"'+d+'"'

    if remote_signing:
        command='{"command":"sign_remote","cert_keyid":"'+certificate_to_use+'","files_to_sign":['+documents_to_sign_full_path+']'
    else:
        command='{"command":"sign_local","cert_fingerprint":"'+certificate_to_use+'","pin":"'+pin+'","files_to_sign":['+documents_to_sign_full_path+']'
    if userid!="":
        command+=',"userid":"'+userid+'"'
    command+='}'
    result_str=api_connect(command)
    if debug: print(result_str)
    result_json=json.loads(result_str)
    if "error" in result_json:
        if debug: print("ERROR: "+result_json['error']) 
        resp_json=result_json
    else:
        resp_json = json.loads(
                str(json.JSONEncoder().encode(result_json['signed_files'])), 
                object_hook=lambda 
                d: SimpleNamespace(**d)
        )        
        execution_without_error=True
    return execution_without_error, resp_json

#shutdown the API
def signer_command_quit():
    debug=False
    execution_without_error=False
    command='{"command":"quit_signer"}'
    result_str=api_connect(command)    
    if debug: print(result_str)

    result_json=json.loads(result_str)
    if "error" in result_json:
        if debug: print("ERROR: "+result_json['error'])            
        execution_without_error=True    
    return execution_without_error


#
# MAIN
#
# Script using the beA.expert SIGNER API as example
#
# here we start ...
#
debug=True

# NEW in version 1.2.2.0 -> port hopping
# check if we get the right port (if port hopping is active)
if False:
    # use the registry
    (execution_without_error, port, lastupdate)=api_get_port("registry")
else:
    # or use the file
    (execution_without_error, port, lastupdate)=api_get_port("file")

if not execution_without_error and port!=-1:
    signer_api_port=port

# (0) get the installed version and version
execution_without_error,signer_version,update_available=signer_command_get_version()
if execution_without_error:
    if debug: print("signer_version",signer_version,"update_available",update_available)
else:
    if debug: print("ERROR: can not get the signer version")

# (1) get if remote or local signing
execution_without_error, remote_signing=signer_command_get_remote()
if execution_without_error:
    if debug: print("remote_signing",remote_signing)
else:
    if debug: print("ERROR: can not determinate if remote or local signing (api_get_signing_remote is missing in the answer)")

# (2) get the certificates linked to this card (remote or local certifificates)
execution_without_error, certificates=signer_command_get_certificates_local_and_remote(remote_signing)
if execution_without_error:
    if debug: print("certificates",certificates)
else:
    if debug: print("ERROR: can not get the certificate(s)") 
    
# (3) choose the cerificate you want to use, Example hier loop over all available cerificates
if execution_without_error:    

    if False:
        # sign only with the first certificate
        nbre=1
    else:
        # OR sign with all certificates for fun ! Most proibaly useless in real life.
        nbre=len(certificates)

    for certificate_to_use_index in range(0,nbre):
        certificate_to_use="" # be sure that no certificate is selected        
        if remote_signing==True:
            certificate_to_use=certificates[certificate_to_use_index].cert_keyid # for remote signing we pass the "cert_keyid" to the signer
        else:
            certificate_to_use=certificates[certificate_to_use_index].cert_fingerprint # for remote signing we pass the "fingerprint" to the signer    

        # (4A) sign the document with the selected certificate        
        if not certificate_to_use=="":
            if debug: print("we will use this certificate :",certificate_to_use, " (remote :",remote_signing,")") 
            
            if False:
                # only one document to sign
                document_to_sign_full_path="readme.txt"
                list_of_document_to_sign_full_path=[]
            else:
                # an array of document to sign (Stapelsignatur)
                document_to_sign_full_path=""
                list_of_document_to_sign_full_path=["readme.txt","readme2.txt","readme3.txt"] #
            
            # last check to see if the file(s) exist(s)
            everything_ok=True
            if document_to_sign_full_path:
                if not os.path.exists(document_to_sign_full_path):
                    everything_ok=False
            else:
                for f in list_of_document_to_sign_full_path:
                    if not os.path.exists(f):
                        everything_ok=False
                        break
            
            # everything seems to be okay, we start the signing process
            if everything_ok:
                # firstly, we will use the licence per certificate with userid=""
                # secondly, we will use the bulk licence mechanism usid a predifined "userid"
                userids={"","TES-00000000000000000000000000000000"} # TEST CASES: with and w/o userid
                for userid in userids:
                    if userid=="":
                        print("no userid to check, licence check will use the certificate number")  
                    else:
                        print("we use the userid ",userid," for the licence check (bulk-licensing)")
                    pin="" # for "local" signing the pin can be passed (NOT FOR REMOTE SIGNING), if empty pin will ba asked on keyreader pad    
                    execution_without_error,signed_documents=signer_command_sign_files(remote_signing,certificate_to_use,pin,userid,document_to_sign_full_path,list_of_document_to_sign_full_path)    
                    if execution_without_error:
                        if debug: 
                            print("signed_documents",signed_documents)                                      
                    else:
                        if debug: 
                            print("ERROR: can not sign the documents(s)")
                            print(signed_documents)                                
            else:
                if debug: print("ERROR: at least one file not found")
        else:
            if debug: print("ERROR: no certificate selected")

    # if we want to end the SIGNER uncomment the following line...
    # execution_without_error=signer_command_quit()

print("That all folks!")
